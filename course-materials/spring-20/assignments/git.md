# Phil's Bare Bones Introduction to Git

We will be using git to manage our shared files.

git is a super-powerful tool: it was designed by the creator of Linux
(Linus Torvalds) to coordinate the work of tens of thousands of Linux
developers across the world. Thankfully, we only need about 0.1% of
what it can do.

The key thing we are going to use it for is keeping track of our
software source code files as they go through different versions. Git
keeps the complete history of everything. So, for example, if you
update an animation and then problems come up, it'll be easy to go
back to the older version (git stores it all).

I looked around for a good tutorial (for people who've never used a
version control system before), and [this seemed to be the best
one](https://hackernoon.com/understanding-git-fcffd87c15a3). Most
resources are written by software developers for software developers
so are very jargony and assume a lot of background knowledge.

You can skip the three sections about branches for now. We won't use
them much in these initial teps. They're a way for people to share
files/work in progress in sub-groups.

## Core git commands

The key commands you will care about are:

`git clone`: create a new copy of a repository (e.g., the SQUARED software, our repository on code.stanford.edu)

`git status`: see what the current status of your files is: have you
changed any, have you made new ones, etc. This basically lets you see
how what's in your working directory is different than what's in the
git database. Use it often! You can also use it on specific files or
directories, e.g., `git status ping.sh` or `git status .`

`git add`: tell git that you're going to add a new file, or update a
file. This doesn't change the database.

`git commit`: take all of the things you've added (with git add) and
actually change your local database with them. This is a single,
atomic update. So people will either see the old files or the new, but
not something in between. This behavior is very important if files are
related and have dependencies!

Note that git commit *DOES NOT UPDATE code.stanford.edu* or the remote
repository you cloned from. It updates your local database. Use it
frequently, when you make some steps of progress and things are in a
good shape. When you are ready to share your updates with everyone
else...

`git pull`: take any updates in the remote database/repository (e.g.,
code.stanford.edu, the SQUARED github) and *apply them to your local
files and database.*

`git push`: take any updates in your local database and apply them to
the remote one, so everyone else can see them. You usually want to
pull before you do this. If you've made some changes, but you're now
behind what's in code.stanford.edu, you want to get the changes in
code.stanford.edu (via pull) before trying to make your own. This
ensures that everything is consistent.

## Running into problems

The problem some of you might run into is when you run `git pull`. The
problem is this: suppose you changed a file on your local machine and
committed it to your local database. You want to push it to
code.stanford.edu so everyone else can see it. But let's say someone
else has updated that file and pushed the update to
code.stanford.edu. Git will say there's a conflict: you've modified
the file, as has someone else, and it might not know how to resolve
it. Which one should it use?

[This explains what to do (how to pick which to keep).](https://codeyarns.com/2016/04/11/how-to-resolve-git-conflict-with-binary-files/)

Also, *read the messages git gives you*. It'll tell you if you need to
add, commit, etc., to make it happy.

Please note that since this is git, whichever you choose, you DO NOT
LOSE THE OTHER FILE. I.e., if you choose to keep your copy, the other
person's copy is still in the database and can be recovered.

If you have more complex git problems, email me! If your local
repository seems to be completely messed up (it happens to everyone!),
you can just clone a new one, and copy files you want to change over
to the new one.
