# Project: LED diffusion

*Instructor Lead: Charles Gadeken*

**Due: Tuesday, January 21st, 2020**

The goals and deliverables for this project are:
  1. Decide the placement and type of LEDs that illuminate the body
  2. Prototype the diffusion mechanism for these LEDs
  3. Develop the physical structure for attaching the LEDs and diffuser
  4. Update top plate CAD model as needed
  5. Build 2 complete diffuers and remove any shadows/visual artifacts

## Body LEDs

The bodies of the shapes will be internally lit. One problem that arises
in this is that we want the body to appear to be a soft, continuous color
or light, and not a set of point sources. If you put some LEDs inside the
body, the body surface will diffuse them a little but not much, and they
will look like diffuse points of light inside.

To give a reasonably uniform luminosity across the body surface, we need
to diffuse the internal LEDs and also place them carefully. Since the body
is not a simple cylinder, this affects LED placement. For example, since
the body is widest in the center and light decays with the square of
the distance, a single line of LEDs will appear dimmer on the surface
of the wide areas than on the surface of the narrow areas.

Determine how you will place LEDs in order to get a reasonably uniform
luminosity across the external body surface, and what type of LEDs you 
will use. The current plan is for the entire shape to be 5V, so please
use 5V LEDs. The luminosity does not have to be perfectly uniform: we
will be able to later refine a bit with software control (making different
LEDs have different brightness). But there should be no dark spots or
large shadows.

Coordinate with the mechanism team to be sure that your LED placement
and their mechanism placement do not overlap (they can both fit).

## Diffusion mechanism

Decide how to diffuse the LEDs such that they will appear as a uniform light
across the body surface. There are two general approaches: diffusing materials
(e.g., white or frosted acrylic) and distance. Read up on diffusion,
refraction, and other light behaviors so you have a clear foundation for
what's happening and can reason about what to use.

## Physical structure

Design the physical structure that will support the LEDs and diffusion
mechanism. Coordinate with the mechanism group as before. This structure
should attach to the top plate somehow, and have a single connector
coming out of it for the LED strip. Design a CAD model for the structure
and build one, attaching it to a top plate. If you need to make holes
in the top plate, coordinate with the body and mechanism teams to make
sure everything fits. If you add holes to the top plate, push those
changes to the project git repository.

## Build 2 diffusers

Build two copies of your diffuser and attach them to the top plate. 
Using existing materials (e.g., plates of white acrylic, paper, etc.), 
mock-up a body. Visually inspect how light illuminates the body. Refine
your design to remove any dark spots or obvious shadows. The resulting
luminosity should seem uniform enough to you and placed such that software
can make up for it. For example, if the wide center is a little dimmer, we
can make this up with software (assuming LED placement allows it); however,
if the sharp point is dark we can't and this requires additional LEDs.


correct






