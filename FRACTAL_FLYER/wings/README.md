# Wing Etchings

This directory contains files for wing etching. Each wing has a fractal
pattern on it:

![Wing fractal](wing-fractal-only.png)

This fractal pattern was generated with the [geomandel](https://github.com/crapp/geomandel) program, using the [make-julia.sh](make-julia.sh) script
in this directory. This Julia set output was then fed through the
[image-to-wing.py](image-to-wing.py) program to crop it and change it from
regions of black to the boundary around regions of black.

The files in this directory are:
- **wing_registry.md**: A listing of the text and icon on each Flyer's wings
- **wing_template_diffusion.svg**: A vector graphics file for cutting and engraving
a single pair of wings. This file is called "diffusion" because there are small
cutouts at the edges of the wings to allow sanding for improved light diffusion.
This diffusion makes the LEDs on the edge of the wing look like a bar of light
rather than a series of pixel points.
- **Wing_Production_Decisions.pdf**: A document describing some of the design
decisions and analysis of the wings, including LED selection, analytical
verification that the wing mechanism has sufficient torque, and how to arrange
cutting wings on a laser cutter.
- **Wing_Laser_Cutting_Instructions.pdf**: Instructions on how to cut out wings
using the (Trotec Laser cutter in Lab64 at Stanford)[https://lab64.stanford.edu/laser-cutter].

Older files:
- **wing_template.svg**: A vector graphics file for cutting and engraving a single
pair of wings.  This version of the wings does not have the cutouts for diffusion.
