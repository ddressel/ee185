package engine;

import java.util.HashMap;
import java.util.Map;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.SysexMessage;

import heronarts.p3lx.ui.component.*;
import heronarts.lx.LX;
import heronarts.lx.LXDeviceComponent;
import heronarts.lx.clip.LXClip;
import heronarts.lx.effect.LXEffect;
import heronarts.lx.midi.LXMidiInput;
import heronarts.lx.midi.LXMidiListener;
import heronarts.lx.midi.LXMidiOutput;
import heronarts.lx.midi.MidiAftertouch;
import heronarts.lx.midi.MidiControlChange;
import heronarts.lx.midi.MidiNote;
import heronarts.lx.midi.MidiNoteOn;
import heronarts.lx.midi.MidiPitchBend;
import heronarts.lx.midi.MidiProgramChange;
import heronarts.lx.midi.surface.APC40Mk2;
import heronarts.lx.midi.surface.LXMidiSurface;
import heronarts.lx.mixer.LXAbstractChannel.CrossfadeGroup;
import heronarts.lx.mixer.LXAbstractChannel;
import heronarts.lx.mixer.LXBus;
import heronarts.lx.mixer.LXChannel;
import heronarts.lx.mixer.LXGroup;
import heronarts.lx.parameter.CompoundParameter;
import heronarts.lx.parameter.LXListenableNormalizedParameter;
import heronarts.lx.parameter.LXParameter;
import heronarts.lx.parameter.LXParameterListener;
import heronarts.lx.pattern.LXPattern;

/** 
 * Ocassionally Processing has a hard time connecting to the APC40 in the right mode.
 * In Ableton Live Mode (the mode we want), the drumpad buttons up top should NOT flash green when pressed.
 * Try restarting the app a couple times until you see the desired behavior.
 * 
 * A lot of this code is lifted from the APC40Mk2 class LXStudio code.
 * 
 * The communications protocol doc might be useful: https://www.akaipro.de/sites/default/files/2018-01/APC40Mk2_Communications_Protocol_v1.2.pdf_7db83a06354c396174676105098e3a7d.pdf
 * User guide: https://www.akaipro.com/amfile/file/download/file/494/product/15/
 * 
 * @author ashleyic@stanford.edu
 */
public class FlightAPC40Listener implements LXMidiListener {
    public APC40Mk2 apc40;
    public LXMidiInput input = null;
    public LXMidiOutput output = null;
    public LX lx;
    private boolean bankOn = true;
    Map<LXAbstractChannel, ChannelListener> channelListeners = new HashMap<LXAbstractChannel, ChannelListener>();
    DeviceListener deviceListener = new DeviceListener();
    UIItemList.ScrollList[] patternLists;
    UIKnob masterHue;
    UIKnob masterSaturation;
    UIKnob masterBrightness;
    UIKnob masterSpeed;
    
    final static byte[] APC_MODE_SYSEX = {
            (byte) 0xf0, // sysex start
            (byte) 0x47, // manufacturers id
            (byte) 0x7f, // device id
            (byte) 0x29, // product model id
            (byte) 0x60, // message
            (byte) 0x00, // bytes MSB
            (byte) 0x04, // bytes LSB
            (byte) 0x42, // Alternate Ableton Live Mode 
            (byte) 0x09, // version maj
            (byte) 0x03, // version min
            (byte) 0x01, // version bugfix
            (byte) 0xf7, // sysex end
    };
    
    public FlightAPC40Listener(LX lx, UIItemList.ScrollList[] patternLists, UIKnob masterHue, UIKnob masterSaturation, UIKnob masterBrightness, UIKnob masterSpeed) {
        this.lx = lx;
        this.patternLists = patternLists;
        this.masterHue = masterHue;
        this.masterSaturation = masterSaturation;
        this.masterBrightness = masterBrightness;
        this.masterSpeed = masterSpeed;
        
        lx.engine.midi.whenReady(() -> {
            if (lx.engine.midi.findSurface("APC40 mkII") == null) {
                return;
            }
            
            this.input = lx.engine.midi.findInput("APC40 mkII");
            input.open();
            input.addListener(this);
            
            this.output = lx.engine.midi.findOutput("APC40 mkII");
            output.open();            
        });
        
        // if no APC40 is detected, just return and ignore this class
        if (input == null || output == null) return;
        
        this.apc40 = new APC40Mk2(lx, input, output);
        try {
            setAPC40Mode();
        } catch (java.lang.UnsatisfiedLinkError e){
            System.out.println("could not set up APC40: unsatisfied link error, caused by mmj on non mac");
            return;
        }
        
        for (LXAbstractChannel channel : this.lx.engine.mixer.channels) {
            ChannelListener channelListener = new ChannelListener(channel);
            channelListeners.put(channel, channelListener);
        }
        
        lx.engine.mixer.focusedChannel.addListener((p) -> {
            sendChannelFocus();
            deviceListener.registerChannel(lx.engine.mixer.getFocusedChannel());
        });      
        deviceListener.registerChannel(lx.engine.mixer.getFocusedChannel());
        
        // set buttons to reflect start up defaults in the UI
        // turn on channel active buttons since all the channels are on by default
        apc40.output.sendNoteOn(0, APC40Mk2.CHANNEL_ACTIVE, APC40Mk2.LED_ON);
        apc40.output.sendNoteOn(1, APC40Mk2.CHANNEL_ACTIVE, APC40Mk2.LED_ON);
        apc40.output.sendNoteOn(2, APC40Mk2.CHANNEL_ACTIVE, APC40Mk2.LED_ON);
        apc40.output.sendNoteOn(3, APC40Mk2.CHANNEL_ACTIVE, APC40Mk2.LED_ON);
        apc40.output.sendNoteOn(4, APC40Mk2.CHANNEL_ACTIVE, APC40Mk2.LED_ON);
        apc40.output.sendNoteOn(5, APC40Mk2.CHANNEL_ACTIVE, APC40Mk2.LED_ON);
        apc40.output.sendNoteOn(6, APC40Mk2.CHANNEL_ACTIVE, APC40Mk2.LED_ON);
        apc40.output.sendNoteOn(7, APC40Mk2.CHANNEL_ACTIVE, APC40Mk2.LED_ON);
        // turn on track select on channel 1
        lx.engine.midi.findSurface("APC40 mkII").output.sendNoteOn(0, APC40Mk2.CHANNEL_FOCUS, APC40Mk2.LED_ON);
        // set top knobs to be like master controls
        apc40.output.sendControlChange(0, APC40Mk2.CHANNEL_KNOB_STYLE, 2 /*APC40Mk2.LED_STYLE_UNIPOLAR*/);
        apc40.output.sendControlChange(0, APC40Mk2.CHANNEL_KNOB_STYLE + 1, 2 /*APC40Mk2.LED_STYLE_UNIPOLAR*/);
        apc40.output.sendControlChange(0, APC40Mk2.CHANNEL_KNOB_STYLE + 2, 2 /*APC40Mk2.LED_STYLE_UNIPOLAR*/);
        apc40.output.sendControlChange(0, APC40Mk2.CHANNEL_KNOB_STYLE + 3, 2 /*APC40Mk2.LED_STYLE_UNIPOLAR*/);
        apc40.output.sendControlChange(0, APC40Mk2.CHANNEL_KNOB_STYLE + 4, 0 /*APC40Mk2.LED_STYLE_OFF*/);
        apc40.output.sendControlChange(0, APC40Mk2.CHANNEL_KNOB_STYLE + 5, 0 /*APC40Mk2.LED_STYLE_OFF*/);
        apc40.output.sendControlChange(0, APC40Mk2.CHANNEL_KNOB_STYLE + 6, 0 /*APC40Mk2.LED_STYLE_OFF*/);
        apc40.output.sendControlChange(0, APC40Mk2.CHANNEL_KNOB_STYLE + 7, 0 /*APC40Mk2.LED_STYLE_OFF*/);
        apc40.output.sendControlChange(0, APC40Mk2.CHANNEL_KNOB, 0);
        apc40.output.sendControlChange(0, APC40Mk2.CHANNEL_KNOB + 1, 127);
        apc40.output.sendControlChange(0, APC40Mk2.CHANNEL_KNOB + 2, 127);
        apc40.output.sendControlChange(0, APC40Mk2.CHANNEL_KNOB + 3, 127);
    }
    
    /**
     * Setting up the startup mode. We do not want the default mode, since that mode won't let us
     * react when the channel is changed. We want a mode where all the buttons are momentary.
     */
    void setAPC40Mode() {
        this.output.sendSysex(APC_MODE_SYSEX);
    }
    
    /**
     * Actions to be taken when buttons on the APC40 are pressed.
     * If a button is unmapped, the ID of that button will be printed in the console.
     */
    public void noteOnReceived(MidiNoteOn note) {
        int channel = note.getChannel();
        int pitch = note.getPitch();
        int velocity = note.getVelocity();
        switch (pitch) {
            case APC40Mk2.CHANNEL_FOCUS:
                int focusedChannel = channel;
                for (int i = 0; i < APC40Mk2.NUM_CHANNELS; ++i) {
                    apc40.output.sendNoteOn(i, APC40Mk2.CHANNEL_FOCUS, (i == focusedChannel) ? APC40Mk2.LED_ON : APC40Mk2.LED_OFF);
                }
                lx.engine.mixer.focusedChannel.setValue(focusedChannel);
                lx.engine.mixer.selectChannel(lx.engine.mixer.getFocusedChannel());
                break;
            case APC40Mk2.MASTER_FOCUS:
                for (int i = 0; i < APC40Mk2.NUM_CHANNELS; ++i) {
                    apc40.output.sendNoteOn(i, APC40Mk2.CHANNEL_FOCUS, APC40Mk2.LED_OFF);
                }
                apc40.output.sendNoteOn(8, APC40Mk2.MASTER_FOCUS, APC40Mk2.LED_ON);
                lx.engine.mixer.focusedChannel.setValue(8);
                lx.engine.mixer.selectChannel(lx.engine.mixer.getFocusedChannel());
                break;
            case APC40Mk2.CHANNEL_ACTIVE:
                if (lx.engine.mixer.getChannel(channel).enabled.getValueb()) {
                    apc40.output.sendNoteOn(channel, APC40Mk2.CHANNEL_ACTIVE, APC40Mk2.LED_OFF);
                } else {
                    apc40.output.sendNoteOn(channel, APC40Mk2.CHANNEL_ACTIVE, APC40Mk2.LED_ON);
                }
                lx.engine.mixer.getChannel(channel).enabled.toggle();
                break;
            case APC40Mk2.CHANNEL_CROSSFADE_GROUP:
                
                if (lx.engine.mixer.getChannel(channel).crossfadeGroup.getEnum() == CrossfadeGroup.BYPASS) {
                    apc40.output.sendNoteOn(channel, APC40Mk2.CHANNEL_CROSSFADE_GROUP, APC40Mk2.LED_ON);
                    lx.engine.mixer.getChannel(channel).crossfadeGroup.setValue(CrossfadeGroup.A);
                } else if (lx.engine.mixer.getChannel(channel).crossfadeGroup.getEnum() == CrossfadeGroup.A) {
                    apc40.output.sendNoteOn(channel, APC40Mk2.CHANNEL_CROSSFADE_GROUP, APC40Mk2.LED_ON + 1);
                    lx.engine.mixer.getChannel(channel).crossfadeGroup.setValue(CrossfadeGroup.B);
                } else {
                    apc40.output.sendNoteOn(channel, APC40Mk2.CHANNEL_CROSSFADE_GROUP, APC40Mk2.LED_OFF);
                    lx.engine.mixer.getChannel(channel).crossfadeGroup.setValue(CrossfadeGroup.BYPASS);
                }
                break;
                
            // first and second columns, wing only patterns    
            case APC40Mk2.CLIP_LAUNCH:
                patternLists[0].setFocusIndex(17); // slow wings!
                break;
            case APC40Mk2.CLIP_LAUNCH + 8:
                patternLists[0].setFocusIndex(21); // snake wings!
                break;
            case APC40Mk2.CLIP_LAUNCH + 16:
                patternLists[0].setFocusIndex(18); // climb wings!
                break;
            case APC40Mk2.CLIP_LAUNCH + 24:
                patternLists[0].setFocusIndex(19); // split wings!
                break;
            case APC40Mk2.CLIP_LAUNCH + 32:   
                patternLists[0].setFocusIndex(20); // vogue wings!
                break;
            case APC40Mk2.CLIP_LAUNCH + 32 + 1:   
                patternLists[0].setFocusIndex(26); // breathing sphere wings!
                break;
            
            // third column, combo patterns
            case APC40Mk2.CLIP_LAUNCH + 0 + 2:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(13); // raspberry!
                break;
            case APC40Mk2.CLIP_LAUNCH + 8 + 2:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(3); // wheel!
                break;
            case APC40Mk2.CLIP_LAUNCH + 16 + 2:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(2); // wave!
                break;
            case APC40Mk2.CLIP_LAUNCH + 24 + 2:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(0); // sweep!
                break;
            
            // 4th, 5th, 6th, 7th columns, LED only patterns
            case APC40Mk2.CLIP_LAUNCH + 32 + 3:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(1); // colorWall!
                break;
            case APC40Mk2.CLIP_LAUNCH + 24 + 3:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(4); // colorGradient!
                break;
            case APC40Mk2.CLIP_LAUNCH + 16 + 3:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(5); // WheelOfColor!
                break;
            case APC40Mk2.CLIP_LAUNCH + 8 + 3:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(6); // Surge!
                break;
            case APC40Mk2.CLIP_LAUNCH + 0 + 3:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(7); // Fractal!
                break;
            case APC40Mk2.CLIP_LAUNCH + 32 + 4:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(9); // rainFall!
                break;
            case APC40Mk2.CLIP_LAUNCH + 24 + 4:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(10); // wingColor!
                break;
            case APC40Mk2.CLIP_LAUNCH + 16 + 4:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(14); // snake!
                break;
            case APC40Mk2.CLIP_LAUNCH + 8 + 4:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(16); // Lighthouse!
                break;
            case APC40Mk2.CLIP_LAUNCH + 0 + 4:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(22); // heartbeat!
                break;
            case APC40Mk2.CLIP_LAUNCH + 32 + 5:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(23); // fireworks!
                break;
            case APC40Mk2.CLIP_LAUNCH + 24 + 5:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(24); // POV!
                break;
            case APC40Mk2.CLIP_LAUNCH + 16 + 5:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(25); // breathingSphere!
                break;
            case APC40Mk2.CLIP_LAUNCH + 8 + 5:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(27); // breathingUp!
                break;
            case APC40Mk2.CLIP_LAUNCH + 0 + 5:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(28); // Fountain!
                break;
            
            // eighth column, LED only patterns that I think work well layered ontop of others
            case APC40Mk2.CLIP_LAUNCH + 32 + 7:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(15); // twinkle!
                break;
            case APC40Mk2.CLIP_LAUNCH + 24 + 7:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(8); // spotlight!
                break;
            case APC40Mk2.CLIP_LAUNCH + 16 + 7:   
                patternLists[lx.engine.mixer.focusedChannel.getValuei()].setFocusIndex(11); // bodyOffset!
                break;
            default:
                System.out.println("note: " + note);
                break;
        }
    }
    
    /**
     * Actions to be taken when buttons on the APC40 are released.
     * Currently nothing is mapped, and the ID of the button will be printed to the console.
     */
    public void noteOffReceived(MidiNote note) {
        int channel = note.getChannel();
        int pitch = note.getPitch();
        switch (pitch) {
            default:
                System.out.println("noteOff: " + note);
                break;
        }
    }
    
    /**
     * Actions to be taken when knobs or sliders are touched.
     * If a knob or slider is unmapped, the ID of that adjuster will be printed in the console.
     */
    public void controlChangeReceived(MidiControlChange cc) {
        int channel = cc.getChannel();
        int value = cc.getValue();
        int ccId = cc.getCC();
        switch (ccId) {
            case APC40Mk2.CHANNEL_FADER:
                lx.engine.mixer.getChannel(channel).fader.setNormalized(cc.getNormalized());
                break;
            case APC40Mk2.MASTER_FADER:
                lx.engine.output.brightness.setNormalized(cc.getNormalized());
                break;
            case APC40Mk2.CROSSFADER:
                lx.engine.mixer.crossfader.setNormalized(cc.getNormalized());
                break;
            case APC40Mk2.DEVICE_KNOB:
            case APC40Mk2.DEVICE_KNOB + 1:
            case APC40Mk2.DEVICE_KNOB + 2:
            case APC40Mk2.DEVICE_KNOB + 3:
            case APC40Mk2.DEVICE_KNOB + 4:
            case APC40Mk2.DEVICE_KNOB + 5:
            case APC40Mk2.DEVICE_KNOB + 6:
            case APC40Mk2.DEVICE_KNOB + 7:
                int knobIndex = ccId - APC40Mk2.DEVICE_KNOB;
                LXListenableNormalizedParameter[] params = ((LXDeviceComponent) ((LXChannel) lx.engine.mixer.getFocusedChannel()).getActivePattern()).getRemoteControls();
                // ignore if the knob touched doesn't apply to the current pattern's parameters
                if (params.length <= knobIndex) {
                    break;
                }
                apc40.output.sendControlChange(0, ccId, value);
                ((LXDeviceComponent) ((LXChannel) lx.engine.mixer.getFocusedChannel()).getActivePattern()).getRemoteControls()[knobIndex].setNormalized(cc.getNormalized());
                break;
            case APC40Mk2.CHANNEL_KNOB:
            case APC40Mk2.CHANNEL_KNOB + 1:
            case APC40Mk2.CHANNEL_KNOB + 2:
            case APC40Mk2.CHANNEL_KNOB + 3:
                int channelKnobIndex = ccId - APC40Mk2.CHANNEL_KNOB;
                apc40.output.sendControlChange(0, ccId, value); 
                switch (channelKnobIndex) {
                    case 0:
                        masterHue.getParameter().setNormalized(cc.getNormalized());
                        break;
                    case 1:
                        masterBrightness.getParameter().setNormalized(cc.getNormalized());
                        break;
                    case 2:
                        masterSaturation.getParameter().setNormalized(cc.getNormalized());
                        break;
                    case 3:
                        masterSpeed.getParameter().setNormalized(cc.getNormalized());
                        break;
                    default:
                        break;
                }
                break;
            default:
                System.out.println("cc: " + cc);
                break;
        }
    }

  public void programChangeReceived(MidiProgramChange pc) {
      System.out.println("pc: " + pc);
  }
  
  public void pitchBendReceived(MidiPitchBend pitchBend) {
      System.out.println("pitchBend: " + pitchBend);
  }
  
  public void aftertouchReceived(MidiAftertouch aftertouch) {
      System.out.println("aftertouch: " + aftertouch);
  }
  
  // the rest of this file is basically copied from the LXStudio APC40Mk2 code
  private void sendChannelPatterns(int index, LXAbstractChannel channelBus) {
      if (index >= APC40Mk2.CLIP_LAUNCH_COLUMNS || !this.bankOn) {
        return;
      }
      if (channelBus instanceof LXChannel) {
        deviceListener.registerChannel(channelBus);
      }
    }

    private void sendChannelClips(int index, LXAbstractChannel channel) {
      if (index >= APC40Mk2.CLIP_LAUNCH_COLUMNS || this.bankOn) {
        return;
      }
      for (int i = 0; i < APC40Mk2.CLIP_LAUNCH_ROWS; ++i) {
        int color = APC40Mk2.LED_OFF;
        int mode = APC40Mk2.LED_MODE_PRIMARY;
        if (channel != null) {
          int pitch = APC40Mk2.CLIP_LAUNCH + index + APC40Mk2.CLIP_LAUNCH_COLUMNS * (APC40Mk2.CLIP_LAUNCH_ROWS - 1 - i);
          LXClip clip = channel.getClip(i);
          if (clip != null) {
            color = channel.arm.isOn() ? APC40Mk2.LED_RED_HALF : APC40Mk2.LED_GRAY;
            if (clip.isRunning()) {
              color = channel.arm.isOn() ? APC40Mk2.LED_RED : APC40Mk2.LED_GREEN;
              apc40.output.sendNoteOn(APC40Mk2.LED_MODE_PRIMARY, pitch, color);
              mode = APC40Mk2.LED_MODE_PULSE;
              color = channel.arm.isOn() ? APC40Mk2.LED_RED_HALF : APC40Mk2.LED_GREEN_HALF;
            }
          }
          apc40.output.sendNoteOn(mode, pitch, color);
        }
      }
    }

    private void sendChannelFocus() {
      int focusedChannel = this.lx.engine.mixer.focusedChannel.getValuei();
      boolean masterFocused = (focusedChannel == this.lx.engine.mixer.channels.size());
      for (int i = 0; i < APC40Mk2.NUM_CHANNELS; ++i) {
          apc40.output.sendNoteOn(i, APC40Mk2.CHANNEL_FOCUS, (!masterFocused && (i == focusedChannel)) ? APC40Mk2.LED_ON : APC40Mk2.LED_OFF);
      }
      apc40.output.sendNoteOn(0, APC40Mk2.MASTER_FOCUS, masterFocused ? APC40Mk2.LED_ON : APC40Mk2.LED_OFF);
    }
  
  private class DeviceListener implements LXParameterListener {

      private LXDeviceComponent device = null;
      private LXEffect effect = null;
      private LXPattern pattern = null;
      private LXBus channel = null;

      private final LXListenableNormalizedParameter[] knobs =
        new LXListenableNormalizedParameter[APC40Mk2.DEVICE_KNOB_NUM];

      DeviceListener() {
        for (int i = 0; i < this.knobs.length; ++i) {
          this.knobs[i] = null;
        }
      }

      void resend() {
        for (int i = 0; i < this.knobs.length; ++i) {
          LXListenableNormalizedParameter parameter = this.knobs[i];
          if (parameter != null) {
              apc40.output.sendControlChange(0, APC40Mk2.DEVICE_KNOB_STYLE + i, parameter.getPolarity() == LXParameter.Polarity.BIPOLAR ? 3/*APC40Mk2.LED_STYLE_BIPOLAR*/ : 2/*APC40Mk2.LED_STYLE_UNIPOLAR*/);
              double normalized = (parameter instanceof CompoundParameter) ?
              ((CompoundParameter) parameter).getBaseNormalized() :
              parameter.getNormalized();
              apc40.output.sendControlChange(0, APC40Mk2.DEVICE_KNOB + i, (int) (normalized * 127));
          } else {
              apc40.output.sendControlChange(0, APC40Mk2.DEVICE_KNOB_STYLE + i, 0/*APC40Mk2.LED_STYLE_OFF*/);
          }
        }
        boolean isEnabled = false;
        if (this.effect != null) {
          isEnabled = this.effect.enabled.isOn();
        } else if (this.pattern != null) {
          isEnabled = this.pattern == ((LXChannel) this.channel).getActivePattern();
        }
        apc40.output.sendNoteOn(0, APC40Mk2.DEVICE_ON_OFF, isEnabled ? APC40Mk2.LED_ON : APC40Mk2.LED_OFF);
      }

      void registerChannel(LXBus channel) {
        if (this.channel != null) {
          if (this.channel instanceof LXChannel) {
            ((LXChannel) this.channel).focusedPattern.removeListener(this);
          }
        }
        this.channel = channel;
        if (channel instanceof LXChannel) {
          ((LXChannel) channel).focusedPattern.addListener(this);
          register(((LXChannel) channel).getActivePattern());
        } else if (channel.effects.size() > 0) {
//          register(channel.getEffect(0));
        } else {
          register(null);
        }
      }

      void registerPrevious() {
        if (this.effect != null) {
          int effectIndex = this.effect.getIndex();
          if (effectIndex > 0) {
            register(this.effect.getBus().getEffect(effectIndex - 1));
          } else if (this.channel instanceof LXChannel) {
            register(((LXChannel) this.channel).getFocusedPattern());
          }
        }
      }

      void registerNext() {
        if (this.effect != null) {
          int effectIndex = this.effect.getIndex();
          if (effectIndex < this.effect.getBus().effects.size() - 1) {
            register(this.effect.getBus().getEffect(effectIndex + 1));
          }
        } else if (this.pattern != null) {
          if (channel.effects.size() > 0) {
            register(channel.getEffect(0));
          }
        }
      }

      void register(LXDeviceComponent device) {
        if (this.device != device) {
          if (this.effect != null) {
            this.effect.enabled.removeListener(this);
          }
          if (this.device != null) {
            for (int i = 0; i < this.knobs.length; ++i) {
              if (this.knobs[i] != null) {
                this.knobs[i].removeListener(this);
                this.knobs[i] = null;
              }
            }
            this.device.controlSurfaceSemaphore.decrement();
          }
          this.pattern = null;
          this.effect = null;
          this.device = device;
          if (this.device instanceof LXEffect) {
            this.effect = (LXEffect) this.device;
            this.effect.enabled.addListener(this);
          } else if (this.device instanceof LXPattern) {
            this.pattern = (LXPattern) this.device;
          }

          int i = 0;
          boolean isEnabled = false;
          if (this.device != null) {
            if (this.effect != null) {
              isEnabled = this.effect.isEnabled();
            } else if (this.pattern != null) {
              isEnabled = this.pattern == ((LXChannel) this.channel).getActivePattern();
            }
            for (LXListenableNormalizedParameter parameter : this.device.getRemoteControls()) {
              if (i >= this.knobs.length) {
                break;
              }
              this.knobs[i] = parameter;
              parameter.addListener(this);
              apc40.output.sendControlChange(0, APC40Mk2.DEVICE_KNOB_STYLE + i, parameter.getPolarity() == LXParameter.Polarity.BIPOLAR ? 3/*APC40Mk2.LED_STYLE_BIPOLAR*/ : 2/*APC40Mk2.LED_STYLE_UNIPOLAR*/);
              double normalized = (parameter instanceof CompoundParameter) ?
                ((CompoundParameter) parameter).getBaseNormalized() :
                parameter.getNormalized();
                apc40.output.sendControlChange(0, APC40Mk2.DEVICE_KNOB + i, (int) (normalized * 127));
              ++i;
            }
            this.device.controlSurfaceSemaphore.increment();
          }
          apc40.output.sendNoteOn(0, APC40Mk2.DEVICE_ON_OFF, isEnabled ? APC40Mk2.LED_ON : APC40Mk2.LED_OFF);
          while (i < this.knobs.length) {
              apc40.output.sendControlChange(0, APC40Mk2.DEVICE_KNOB_STYLE + i, 0/*APC40Mk2.LED_STYLE_OFF*/);
            ++i;
          }
        }
      }

      @Override
      public void onParameterChanged(LXParameter parameter) {
          if ((this.channel != null) &&
                  (this.channel instanceof LXChannel) &&
                  (parameter == ((LXChannel)this.channel).focusedPattern)) {
              if ((this.device == null) || (this.device instanceof LXPattern)) {
                  register(((LXChannel) this.channel).getFocusedPattern());
              }
          } else if ((this.effect != null) && (parameter == this.effect.enabled)) {
              apc40.output.sendNoteOn(0, APC40Mk2.DEVICE_ON_OFF, this.effect.enabled.isOn() ? APC40Mk2.LED_ON : APC40Mk2.LED_OFF);
          } else {
              for (int i = 0; i < this.knobs.length; ++i) {
                  if (parameter == this.knobs[i]) {
                      double normalized = (parameter instanceof CompoundParameter) ?
                              ((CompoundParameter) parameter).getBaseNormalized() :
                                  this.knobs[i].getNormalized();
                              apc40.output.sendControlChange(0, APC40Mk2.DEVICE_KNOB + i, (int) (normalized * 127));
                              break;
                  }
              }
          }
      }

      void onDeviceOnOff() {
        if (this.pattern != null) {
          this.pattern.getChannel().goPatternIndex(this.pattern.getIndex());
          apc40.output.sendNoteOn(0, APC40Mk2.DEVICE_ON_OFF, 1);
        } else if (this.effect != null) {
          this.effect.enabled.toggle();
        }
      }

      void onKnob(int index, double normalized) {
        if (this.knobs[index] != null) {
          this.knobs[index].setNormalized(normalized);
        }
      }


    }

  private class ChannelListener implements LXChannel.Listener, LXBus.ClipListener, LXParameterListener {

      private final LXAbstractChannel channel;

      ChannelListener(LXAbstractChannel channel) {
        this.channel = channel;
        if (channel instanceof LXChannel) {
          ((LXChannel) channel).addListener(this);
        } else {
          channel.addListener(this);
        }
        channel.addClipListener(this);
        channel.cueActive.addListener(this);
        channel.enabled.addListener(this);
        channel.crossfadeGroup.addListener(this);
        channel.arm.addListener(this);
        if (channel instanceof LXChannel) {
          LXChannel c = (LXChannel) channel;
          c.focusedPattern.addListener(this);
          c.controlSurfaceFocusLength.setValue(APC40Mk2.CLIP_LAUNCH_ROWS);
          int focusedPatternIndex = c.getFocusedPatternIndex();
          c.controlSurfaceFocusIndex.setValue(focusedPatternIndex < APC40Mk2.CLIP_LAUNCH_ROWS ? 0 : (focusedPatternIndex - APC40Mk2.CLIP_LAUNCH_ROWS + 1));
        }
      }

      public void dispose() {
        if (channel instanceof LXChannel) {
          ((LXChannel) channel).removeListener(this);
        } else {
          channel.removeListener(this);
        }
        this.channel.removeListener(this);
        this.channel.removeClipListener(this);
        this.channel.cueActive.removeListener(this);
        this.channel.enabled.removeListener(this);
        this.channel.crossfadeGroup.removeListener(this);
        this.channel.arm.removeListener(this);
        if (this.channel instanceof LXChannel) {
          LXChannel c = (LXChannel) this.channel;
          c.focusedPattern.removeListener(this);
          c.controlSurfaceFocusLength.setValue(0);
          c.controlSurfaceFocusIndex.setValue(0);
        }
        for (LXClip clip : this.channel.clips) {
          if (clip != null) {
            clip.running.removeListener(this);
          }
        }
      }

      public void onParameterChanged(LXParameter p) {
        int index = this.channel.getIndex();
        if (index >= APC40Mk2.CLIP_LAUNCH_COLUMNS) {
          return;
        }

        if (p == this.channel.cueActive) {
            apc40.output.sendNoteOn(index, APC40Mk2.CHANNEL_SOLO, this.channel.cueActive.isOn() ? APC40Mk2.LED_ON : APC40Mk2.LED_OFF);
        } else if (p == this.channel.enabled) {
            apc40.output.sendNoteOn(index, APC40Mk2.CHANNEL_ACTIVE, this.channel.enabled.isOn() ? APC40Mk2.LED_ON : APC40Mk2.LED_OFF);
        } else if (p == this.channel.crossfadeGroup) {
            apc40.output.sendNoteOn(index, APC40Mk2.CHANNEL_CROSSFADE_GROUP, this.channel.crossfadeGroup.getValuei());
        } else if (p == this.channel.arm) {
            apc40.output.sendNoteOn(index, APC40Mk2.CHANNEL_ARM, this.channel.arm.isOn() ? APC40Mk2.LED_ON : APC40Mk2.LED_OFF);
          sendChannelClips(this.channel.getIndex(), this.channel);
        } else if (p.getParent() instanceof LXClip) {
          // TODO(mcslee): could be more efficient...
          sendChannelClips(index, this.channel);
        }
        if (this.channel instanceof LXChannel) {
          LXChannel c = (LXChannel) this.channel;
          if (p == c.focusedPattern) {
            int focusedPatternIndex = c.getFocusedPatternIndex();
            int channelSurfaceIndex = c.controlSurfaceFocusIndex.getValuei();
            if (focusedPatternIndex < channelSurfaceIndex) {
              c.controlSurfaceFocusIndex.setValue(focusedPatternIndex);
            } else if (focusedPatternIndex >= channelSurfaceIndex + APC40Mk2.CLIP_LAUNCH_ROWS) {
              c.controlSurfaceFocusIndex.setValue(focusedPatternIndex - APC40Mk2.CLIP_LAUNCH_ROWS + 1);
            }
            sendChannelPatterns(index, c);
          }
        }
      }

      @Override
      public void effectAdded(LXBus channel, LXEffect effect) {
      }

      @Override
      public void effectRemoved(LXBus channel, LXEffect effect) {
      }

      @Override
      public void effectMoved(LXBus channel, LXEffect effect) {
        // TODO(mcslee): update device focus??
      }

      @Override
      public void indexChanged(LXAbstractChannel channel) {
        // Handled by the engine channelMoved listener.
      }

      @Override
      public void groupChanged(LXChannel channel, LXGroup group) {

      }

      @Override
      public void patternAdded(LXChannel channel, LXPattern pattern) {
        sendChannelPatterns(channel.getIndex(), channel);
      }

      @Override
      public void patternRemoved(LXChannel channel, LXPattern pattern) {
        sendChannelPatterns(channel.getIndex(), channel);
      }

      @Override
      public void patternMoved(LXChannel channel, LXPattern pattern) {
        sendChannelPatterns(channel.getIndex(), channel);
      }

      @Override
      public void patternWillChange(LXChannel channel, LXPattern pattern, LXPattern nextPattern) {
        sendChannelPatterns(channel.getIndex(), channel);
      }

      @Override
      public void patternDidChange(LXChannel channel, LXPattern pattern) {
        sendChannelPatterns(channel.getIndex(), channel);
      }

      @Override
      public void clipAdded(LXBus bus, LXClip clip) {
        clip.running.addListener(this);
        sendChannelClips(this.channel.getIndex(), this.channel);
      }

      @Override
      public void clipRemoved(LXBus bus, LXClip clip) {
        clip.running.removeListener(this);
        sendChannelClips(this.channel.getIndex(), this.channel);
      }

    }
}


