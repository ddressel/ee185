#!/usr/bin/env python

# Script to test out flight simulator
# Run flight simulator then run this python script
import socket, time, random

s = socket.socket()
s.connect(('127.0.0.1',8080))

FRAME_DELAY = 0.0005

while True:
    # Create some random colours to test out wing LEDs
    color = str(random.randint(0,255)) + "," + str(random.randint(0, 255)) + "," + str(random.randint(0, 255))
    color2 = str(random.randint(0,255)) + "," + str(random.randint(0, 255)) + "," + str(random.randint(0, 255))
    color3 = str(random.randint(0,255)) + "," + str(random.randint(0, 255)) + "," + str(random.randint(0, 255))
    for i in range(77):
    	x = str(i) + ":3:0:" + str(random.randint(-90,90))
    	s.send(x.encode());
    	time.sleep(FRAME_DELAY)
    	x = str(i) + ":3:1:" + str(random.randint(-90,90))
    	s.send(x.encode());
    	x = str(i) + ":2:0:" + color + "," + color2 + "," + color3
    	s.send(x.encode());
    	time.sleep(FRAME_DELAY)
    	x = str(i) + ":2:1:" + color + "," + color2 + "," + color3
    	s.send(x.encode());
    	time.sleep(FRAME_DELAY)
    	x = str(i) + ":1:" + color
    	s.send(x.encode());
    	time.sleep(FRAME_DELAY)

s.close()