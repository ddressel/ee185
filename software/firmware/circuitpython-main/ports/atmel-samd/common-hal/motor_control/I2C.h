#ifndef MICROPY_INCLUDED_ATMEL_SAMD_COMMON_HAL_MOTOR_CONTROL_I2C_H
#define MICROPY_INCLUDED_ATMEL_SAMD_COMMON_HAL_MOTOR_CONTROL_I2C_H

#include "shared-bindings/adafruit_bus_device/I2CDevice.h"
#include "shared-bindings/busio/I2C.h"
#include "peripherals/samd/pins.h"
#include "shared-bindings/motor_control/__init__.h"
#include "ports/atmel-samd/common-hal/motor_control/Control.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
  busio_i2c_obj_t* i2c;
  uint8_t addr;
} i2c_sensor_t;

i2c_sensor_t i2c_sensor_create(uint8_t addr);
void i2c_read_register(i2c_sensor_t device, uint8_t reg, uint8_t* buf, uint8_t len);
uint8_t i2c_read_register_byte(i2c_sensor_t sensor, uint8_t reg);
void i2c_write_register_byte(i2c_sensor_t sensor, uint8_t reg, uint8_t val);

#endif

