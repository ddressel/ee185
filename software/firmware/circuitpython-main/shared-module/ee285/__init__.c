#include <stdio.h>

#include "py/runtime.h"
#include "py/mphal.h"

#include "ports/atmel-samd/asf4/samd51/hal/include/hal_gpio.h"
#include "common-hal/microcontroller/Pin.h"

#include "shared-bindings/neopixel_write/__init__.h"
#include "shared-bindings/microcontroller/Pin.h"
#include "shared-module/ee285/__init__.h"

#include "ports/atmel-samd/common-hal/motor_control/PID.h"
#include "ports/atmel-samd/common-hal/motor_control/Control.h"
#include "ports/atmel-samd/common-hal/motor_control/Accel.h"

#define FIXED 4

#define NUM_WING_LEDS 72 //Number of LEDs per wing

#define NUM_BODY_LEDS 85 //Number of LEDs in the body

//Offsets for LED interpolation use LED # in the strip, 0 indexed
//The implementation requires offset_1 < offset_2 < offset_3 < NUM_BODY_LEDS - 1
#define OFFSET_1 0 
#define OFFSET_2 45
#define OFFSET_3 70


static void led_gradient (uint8_t grb_array[], uint8_t color1[], uint8_t color2[], uint8_t start, uint8_t end, uint8_t size) {
  // grb_array is an instantiated array of size num_leds*3 with structure
  // { g led_0, r led_0, b led_0, g led_1, r led_1, b led_1, ... }

  // set first led color and last led color
	grb_array[start] = color1[0];
	grb_array[start + 1] = color1[1];
	grb_array[start + 2] = color1[2];


	grb_array[end % size]   = color2[0];
	grb_array[end % size +1] = color2[1];
	grb_array[end % size +2] = color2[2];
	if (start + 1 > end) return; //There are either 2 or 1 leds to set, no interpolation

	uint8_t i;
	int16_t increment[3];
  uint8_t num_leds = (end - start)/3;

  // convert value of 1st led to fixed point
	int16_t g = color1[0] << FIXED;
	int16_t r = color1[1] << FIXED;
	int16_t b = color1[2] << FIXED;

  // calulate the gradient for each color led
	increment[0] = (( color2[0] << FIXED ) - g) / (num_leds);
	increment[1] = (( color2[1] << FIXED ) - r) / (num_leds);
	increment[2] = (( color2[2] << FIXED ) - b) / (num_leds);

  // fill in array using the gradient
	for ( i = 3; i < num_leds*3; i+=3 ) {
		g += increment[0];
		r += increment[1];
		b += increment[2];
		grb_array[(start + i + 0) % size] = (uint8_t) (g >> FIXED);
		grb_array[(start + i + 1) % size] = (uint8_t) (r >> FIXED);
		grb_array[(start + i + 2) % size] = (uint8_t) (b >> FIXED);
	}

	return;
}

void shared_module_ee285_body_leds(mp_obj_t rgb)
{
  mp_obj_t* items;
  size_t len;

  //Get the rgb values from the arrays
  mp_obj_get_array(rgb, &len, &items);
  if (len < 3 || len > 4)
  {
    mp_raise_ValueError_varg(translate("Expected tuple of length %3, got %d"), len);
  }
  uint8_t GRB[3]; 
  GRB[0] = mp_obj_get_int(items[1]); //This is needed to convert from python ints to C ints
  GRB[1] = mp_obj_get_int(items[0]);
  GRB[2] = mp_obj_get_int(items[2]);

  uint8_t pixels[NUM_BODY_LEDS*3];

  //Set all GRB values to the input
  for (int i = 0; i < NUM_BODY_LEDS; i++) //This doesn't modify the rgb values
  {
    pixels[3*i + 0] = GRB[0];
    pixels[3*i + 1] = GRB[1];
    pixels[3*i + 2] = GRB[2];
  }

  mcu_pin_obj_t pin;
  pin.number = PIN_PA04; //A4 on m4 express, see ports/atmel-samd/boards/[board]/pins.c for other pins
  
  //Set up the pin
  gpio_set_pin_direction(pin.number, GPIO_DIRECTION_OUT);
  gpio_set_pin_pull_mode(pin.number, GPIO_PULL_OFF);
  digitalio_digitalinout_obj_t io;
  io.pin = &pin;

  //Output the array to the LEDs,
  //this turns off interrupts then turns them back on after it finishes
  common_hal_neopixel_write(&io, pixels, NUM_BODY_LEDS*3);

}

void shared_module_ee285_wing_leds(int side, mp_obj_t rgb1, mp_obj_t rgb2, mp_obj_t rgb3) {
  mp_obj_t* items;
  size_t len;

  //Get the rgb values from the arrays
  mp_obj_get_array(rgb1, &len, &items);
  if (len < 3 || len > 4)
  {
    mp_raise_ValueError_varg(translate("Expected tuple of length %3, got %d"), len);
  }
  uint8_t GRB1[3]; 
  GRB1[0] = mp_obj_get_int(items[1]);
  GRB1[1] = mp_obj_get_int(items[0]);
  GRB1[2] = mp_obj_get_int(items[2]);

  mp_obj_get_array(rgb2, &len, &items);
  if (len < 3 || len > 4)
  {
    mp_raise_ValueError_varg(translate("Expected tuple of length %3, got %d"), len);
  }
  uint8_t GRB2[3];
  GRB2[0] = mp_obj_get_int(items[1]);
  GRB2[1] = mp_obj_get_int(items[0]);
  GRB2[2] = mp_obj_get_int(items[2]);

  mp_obj_get_array(rgb3, &len, &items);
  if (len < 3 || len > 4)
  {
    mp_raise_ValueError_varg(translate("Expected tuple of length %3, got %d"), len);
  }
  uint8_t GRB3[3];
  GRB3[0] = mp_obj_get_int(items[1]);
  GRB3[1] = mp_obj_get_int(items[0]);
  GRB3[2] = mp_obj_get_int(items[2]);
  

  uint8_t pixels[NUM_WING_LEDS*3];

  //Offset 1 to 2
  led_gradient (pixels, GRB1, GRB2, OFFSET_1*3, OFFSET_2*3, (NUM_WING_LEDS-1)*3);
  //Offset 2 to 3
  led_gradient (pixels, GRB2, GRB3, OFFSET_2*3, OFFSET_3*3, (NUM_WING_LEDS -1)*3);
  //This interpolation wraps around by treating pixels as a bigger array than it is
  led_gradient (pixels, GRB3, GRB1, OFFSET_3*3, (NUM_WING_LEDS + OFFSET_1)*3, (NUM_WING_LEDS)*3);


  mcu_pin_obj_t pin;
  pin.number = side == 1 ? PIN_PA02 : PIN_PB08; //A0 and A2 on the feather m4, look up in a pins.h for other boards
  
  gpio_set_pin_direction(pin.number, GPIO_DIRECTION_OUT);
  gpio_set_pin_pull_mode(pin.number, GPIO_PULL_OFF);
  digitalio_digitalinout_obj_t io;
  io.pin = &pin;

  common_hal_neopixel_write(&io, pixels, NUM_WING_LEDS*3);
}

static bool motor_control_initted = false;

void shared_module_ee285_motor_control_loop_init(void) {
  if(motor_control_initted) {
    return;
  }
  
  // Init accels
  init_accels();

  // Stretch goal: refactor PWM so that it has an explicit init, and then init 
  // PWM here

  // Init interrupts
  common_hal_motor_construct();
  common_hal_motor_PID();

  motor_control_initted = true;
}

void shared_module_ee285_wing_angle(int side, float angle)
{
  if(!motor_control_initted) {
    printf("Motor control loop not yet initted! Init first!\n");
    return;
  }
  control_set_degrees(side, angle);
  return;
}

void shared_module_ee285_test(void)
{
  serial_write_compressed(translate("This is the ee285 test function. \n Edit\
  me in `software/firmware/circuitpython-main/shared-module/ee285/__init__.c`\
  \n"));
}